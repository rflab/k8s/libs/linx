{
  _config:: {
    linx: {
      name: "linx",
      image: "andreimarcu/linx-server",
      port: 8080,
      domain: "",
      tlsSecret : "linx-cert",
      config: {'linx.config': ""},
    }
  },
  local k = import "ksonnet-util/kausal.libsonnet",
  local deploy = k.apps.v1.deployment,
  local container = k.core.v1.container,
  local port = k.core.v1.containerPort,
  local service = k.core.v1.service,
  local c = $._config,

  linx: {
    deployment: deploy.new(
      name=c.linx.name, replicas=1,
      containers=[
        container.new(c.linx.name, c.linx.image)
        + container.withPorts([port.newNamed(c.linx.port, "web")])
        + container.withArgs(["-config", "/config/linx.config"])
      ]
    )
    + deploy.spec.selector.withMatchLabels({"app":c.linx.name})
    + deploy.metadata.withLabels({"app": c.linx.name})
    + deploy.spec.template.metadata.withLabels({"app": c.linx.name})
    + k.util.configMapVolumeMount($.linx.configMap, "/config"),

    service: k.util.serviceFor(self.deployment)
    + service.spec.withType("ClusterIP")
    + service.spec.withSelector({"app": c.linx.name})
    + service.metadata.withName(c.linx.name)
    + service.metadata.withLabels({"app": c.linx.name}),

    local ingress = k.networking.v1.ingress,
    local ingressTLS = k.networking.v1.ingressTLS,
    local ingressRule = k.networking.v1.ingressRule,
    local ingressPath = k.networking.v1.httpIngressPath,
    ingress: ingress.new(c.linx.name)
    + ingress.metadata.withAnnotations({"cert-manager.io/cluster-issuer": "letsencrypt-prod"})
    + ingress.spec.withTls(
      ingressTLS.withHosts(c.linx.domain)
      + ingressTLS.withSecretName(c.linx.tlsSecret)
    )
    + ingress.spec.withRules(
      ingressRule.withHost(c.linx.domain)
      + ingressRule.http.withPaths([
        ingressPath.withPath("/")
        + ingressPath.withPathType("Prefix")
        + ingressPath.backend.service.withName(self.service.metadata.name)
        + ingressPath.backend.service.port.withNumber(self.service.spec.ports[0].port)
        ])
    ),
    local configMap = k.core.v1.configMap,
    configMap: configMap.new(c.linx.name, c.linx.config),
  }
}
