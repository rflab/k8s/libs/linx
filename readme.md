# Linx

Sets up a linx deployment with ingress. This does not use a volume for storing whatever is uploaded to it.

## Usage:

### Install
```sh
jb install git@gitlab.com:rflab/k8s/libs/linx.git
```

### Config file

This is optional but recommended, it reads it into `_config.linx.config`. See
minimal configuration below.

### Minimal configuration

```jsonnet
(import "gitlab.com/rflab/k8s/libs/linx/linx.libsonnet")
{
  _config+:: {
    linx+:{
      domain: "example.com"
      config: {'linx.config': importstr 'linx.config'}
    }
  }
}

```
